from collections import deque


class EditeurTexte:
    def __init__(self):
        self.texte = deque()
        self.annules = deque()

    def ecrire(self, element):
        self.texte.append(element)
        self.annules.clear()

    def annuler(self):
        if self.texte:
            dernier = self.texte.pop()
            self.annules.append(dernier)

    def retablir(self):
        if self.annules:
            dernier = self.annules.pop()
            self.texte.append(dernier)

    def afficher(self):
        return "".join(self.texte)
