from .stack import Stack


class EditeurTexte:
    def __init__(self):
        self.texte = Stack()
        self.annules = Stack()

    def ecrire(self, element):
        self.texte.push(element)
        self.annules = Stack()

    def annuler(self):
        if not self.texte.is_empty():
            dernier = self.texte.pop()
            self.annules.push(dernier)

    def retablir(self):
        if not self.annules.is_empty():
            dernier = self.annules.pop()
            self.texte.push(dernier)

    def afficher(self):
        current = self.texte.head
        elements = []
        while current is not None:
            elements.append(current.getData())
            current = current.getNext()
        elements.reverse()
        return "".join(elements)
