from .node import Node


class Stack:
    def __init__(self):
        self.head = None  # Initialement, la pile est vide

    def push(self, elem):
        temp = Node(elem)
        temp.setNext(self.head)  # Le nouveau nœud pointe vers l'ancien sommet
        self.head = temp  # Le nouveau nœud devient le sommet de la pile

    def pop(self):
        if self.is_empty():
            return None  # Rien à dépiler si la pile est vide
        popped = self.head.getData()  # la valeur du sommet
        self.head = self.head.getNext()  # Le sommet devient le nœud suivant
        return popped

    def is_empty(self):
        return self.head is None  # Retourne True si la pile est vide

    def sommet(self):
        if self.is_empty():
            return None
        return self.head.getData()

    def afficher(self):
        current = self.head
        elements = []
        while current is not None:
            elements.append(current.getData())
            current = current.getNext()
        return " -> ".join(map(str, elements))
