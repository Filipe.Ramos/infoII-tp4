from .pile import Pile


class EditeurTexte:
    def __init__(self):
        self.texte = Pile()  # Pile ecrits
        self.annules = Pile()  # Pile annulés

    def ecrire(self, element):
        self.texte.empiler(element)
        # On vide la pile des annulés car on écrit un nouvel élément
        self.annules = Pile()

    def annuler(self):
        if not self.texte.est_vide():
            dernier = self.texte.depiler()
            self.annules.empiler(dernier)

    def retablir(self):
        if not self.annules.est_vide():
            dernier = self.annules.depiler()
            self.texte.empiler(dernier)

    def afficher(self):
        # on retourne tous les elements concaténés
        return "".join(self.texte.elements)
