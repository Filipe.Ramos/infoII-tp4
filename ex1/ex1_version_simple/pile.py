class Pile:
    def __init__(self):
        self.elements = []

    def est_vide(self):
        return len(self.elements) == 0

    def empiler(self, element):
        self.elements.append(element)

    def depiler(self):
        if not self.est_vide():
            return self.elements.pop()
        return None

    def sommet(self):
        if not self.est_vide():
            return self.elements[-1]  # dernière occurence de la liste => tête
        return None
