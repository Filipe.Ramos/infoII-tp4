class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        priority, value = item
        # On cherche la bonne position pour insérer l'élément en fonction de sa priorité
        index = 0
        while index < len(self.items) and self.items[index][0] <= priority:
            index += 1
        self.items.insert(index, item)

    def dequeue(self):
        if not self.is_empty():
            return self.items.pop(0)  # Retire le premier élément
        return None

    def is_empty(self):
        return self.items == []

    def size(self):
        return len(self.items)


file = File()
file.enqueue((4, "Nathan"))
file.enqueue((2, "Julia"))
file.enqueue((1, "Amandine"))
file.enqueue((3, "Mathias"))

print("Contenu de la file (ordre de priorité décroissant) :")
while not file.is_empty():
    print(file.dequeue())
