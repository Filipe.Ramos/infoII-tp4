#TP4 Info II

Ce projet implémente un éditeur de texte interactif en Python pour les exercices du TP Info II, avec différentes versions pour tester les fonctionnalités de piles et de files d'attente. Le fichier `main.py` contient l'éditeur de texte pour les exercices 1 et 2, et le fichier `infoII_tp4_ex3.py` contient l'implémentation de la file d'attente prioritaire pour l'exercice 3.

## Instructions d'utilisation

### Exercice 1 : Editeur de Texte avec Pile (TAD ou simple)

Pour l'exercice 1, le fichier `main.py` propose deux versions de la pile :

1. **Version TAD** : basée sur une implémentation de pile en style C avec des `Node` et `Stack`.
2. **Version simple** : utilisant une structure de pile plus classique avec une liste.

**Choix de la version à utiliser :**

1. Ouvrez le fichier `main.py`.
2. Vous verrez trois lignes d'import en commentaire en haut du fichier :
   ```python
   # from ex1.ex1_TAD import EditeurTexte
   from ex1.ex1_version_simple import EditeurTexte
   # from ex2.editeur_de_texte_deque import EditeurTexte
   ```
3. Pour utiliser la **version TAD**, décommentez la ligne `from ex1.ex1_TAD import EditeurTexte` et commentez les autres.
4. Pour utiliser la **version simple**, décommentez la ligne `from ex1.ex1_version_simple import EditeurTexte`.
5. Si vous souhaitez tester **l'exercice 2** avec `collections.deque`, décommentez `from ex2.editeur_de_texte_deque import EditeurTexte` et commentez les lignes de l'exercice 1.

**Exécution :**

- Une fois que la version souhaitée est sélectionnée dans `main.py`, exécutez la commande suivante dans le terminal :
  ```bash
  python3 main.py
  ```

### Exercice 2 : Editeur de Texte avec `collections.deque`

Pour tester l'éditeur de texte en utilisant `collections.deque`, vous pouvez :

1. Ouvrir `main.py`.
2. Décommenter la ligne `from ex2.editeur_de_texte_deque import EditeurTexte`.
3. Commenter les autres lignes d'import de l'exercice 1.
4. Exécuter le fichier en lançant :
   ```bash
   python3 main.py
   ```

### Exercice 3 : File d'attente prioritaire

Pour tester la file d'attente prioritaire (exercice 3), il suffit de lancer directement le fichier `infoII_tp4_ex3.py` :

```bash
python3 infoII_tp4_ex3.py
```

## Commandes disponibles dans l'éditeur de texte interactif

Lors de l'exécution de `main.py` (exercices 1 ou 2), vous aurez accès aux commandes suivantes :

- **Tapez du texte directement** pour l'ajouter.
- **`annuler`** : annule la dernière entrée.
- **`afficher`** : affiche le texte actuel.
- **`quitter`** : quitte l'éditeur de texte.
