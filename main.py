# from ex1.ex1_TAD import EditeurTexte
from ex1.ex1_version_simple import EditeurTexte

# from ex2.editeur_de_texte_deque import EditeurTexte

"""
comente la version simple et décommente l'autre pour utiliser la version TAD "style c" 
fais de même avec ex2 pour utiliser collections.deque  
"""


def editeur_interactif():
    editeur = EditeurTexte()

    print("Bienvenue dans l'éditeur de texte interactif.")
    print("Commandes :")
    print("  tapez du texte directement pour l'ajouter")
    print("  tapez 'annuler' pour annuler la dernière entrée")
    print("  tapez 'afficher' pour afficher le texte actuel")
    print("  tapez 'quitter' pour sortir de l'éditeur")

    while True:
        # Capture de l'entrée utilisateur
        commande = input("\n> ").strip()

        if commande == "quitter":
            print("Au revoir!")
            break
        elif commande == "annuler":
            editeur.annuler()
            print(f"Texte actuel : {editeur.afficher()}")
        elif commande == "afficher":
            print(f"Texte actuel : {editeur.afficher()}")
        else:
            editeur.ecrire(commande)
            print(f"Texte actuel : {editeur.afficher()}")


if __name__ == "__main__":
    editeur_interactif()
